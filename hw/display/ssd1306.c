/*
 * SSD1306 OLED controller with 128x32 display. (128x64)
 *
 * Copyright (c) 2024.
 * Written by Ryan Mamone
 *
 * This code is licensed under the GPL.
 */

/* The controller can support a variety of different displays, but we only
   implement one.  Most of the commands relating to brightness and geometry
   setup are ignored. */


// i2ctransfer -y 1 w3@0x3c 0x80 0x81 0x00

#include "qemu/osdep.h"
#include "hw/i2c/i2c.h"
#include "migration/vmstate.h"
#include "qemu/module.h"
#include "ui/console.h"
#include "qom/object.h"

#define DEBUG_SSD1306 1

#ifdef DEBUG_SSD1306
#define DPRINTF(fmt, ...) \
do { printf("ssd1306: " fmt , ## __VA_ARGS__); } while (0)
#define BADF(fmt, ...) \
do { fprintf(stderr, "ssd1306: error: " fmt , ## __VA_ARGS__);} while (0)
#else
#define DPRINTF(fmt, ...) do {} while(0)
#define BADF(fmt, ...) \
do { fprintf(stderr, "ssd1306: error: " fmt , ## __VA_ARGS__);} while (0)
#endif

/* Max display dimensions */
#define WIDTH 128
#define HEIGHT 64

/* Scaling factor for pixels.  */
#define SCALE_FACTOR 4
#define FB_WIDTH (WIDTH * SCALE_FACTOR)
#define FB_HEIGHT (HEIGHT * SCALE_FACTOR)

void printhex(void* buf, uint32_t count, uint32_t width);
void printhex(void* buf, uint32_t count, uint32_t width)
{
    uint8_t *tmp = (uint8_t *)buf;
    for (int i = 0; i < count; i++)
    {
        printf("%02x", tmp[i]);
        if(i % width == (width - 1))
            printf("\n");
    }
}


enum ssd1306_mode
{
    ssd1306_IDLE,
    ssd1306_DATA,
    ssd1306_CMD,
    ssd1306_CMD_DATA
};

#define TYPE_SSD1306 "ssd1306"
OBJECT_DECLARE_SIMPLE_TYPE(ssd1306_state, SSD1306)

struct ssd1306_state {
    I2CSlave parent_obj;

    QemuConsole *con;
    int row;
    int col;
    int start_line;
    int mirror;
    int flash;
    int enabled;
    int inverse;
    int redraw;
    enum ssd1306_mode mode;
    uint8_t cmd; /* Command ID bytes */
    uint8_t cmd_byte_num; /* Command data parameter number */
    uint8_t framebuffer[WIDTH * HEIGHT];
};

static uint8_t ssd1306_recv(I2CSlave *i2c)
{
    BADF("Reads not implemented\n");
    return 0xff;
}

static int ssd1306_send(I2CSlave *i2c, uint8_t data)
{
    ssd1306_state *s = SSD1306(i2c);

    switch (s->mode) {
    case ssd1306_IDLE:
        //DPRINTF("byte 0x%02x\n", data);
        if (data == 0x80)
            s->mode = ssd1306_CMD;
        else if (data == 0x40)
            s->mode = ssd1306_DATA;
        else
            BADF("Unexpected byte 0x%x\n", data);
        break;
    case ssd1306_DATA:
        //DPRINTF("data 0x%02x\n", data);
        /* Horizontal Addressing Mode */
        for (int i = 0; i < 8; i++)
        {    
            s->framebuffer[s->col++ + s->row * WIDTH] = ((data << i) & 0x80) == 0x80 ? 0xff : 0x00;
            if (s->col == WIDTH)
            {
                s->col = 0;
                s->row++;
                if (s->row == HEIGHT)
                    s->row = 0;
            }
        }
        s->redraw = 1;
#if 0
        /* Page Mode */
        if (s->col == WIDTH)
        {
            s->col = 0;
        }
        /* TODO: Vertical Addressing Mode */
#endif
        break;
    case ssd1306_CMD:
            s->cmd = data;
            s->cmd_byte_num = 0;
            //DPRINTF("cmd 0x%02x\n", data);
            __attribute__ ((fallthrough));
    case ssd1306_CMD_DATA:
        switch (s->cmd) {
        case 0x81: /* Set Contrast Control */
            if (s->cmd_byte_num == 0)
                s->mode = ssd1306_CMD_DATA;
            else
            {
                data = data;
                s->mode = ssd1306_IDLE;
            }
            break;
        case 0xA4: /* Entire display off (Use GDDRAM) */
            s->flash = 0;
            s->redraw = 1;
            s->mode = ssd1306_IDLE;
            break;
        case 0xA5: /* Entire display on (Ignore GDDRAM) */
            s->flash = 1;
            s->redraw = 1;
            s->mode = ssd1306_IDLE;
            break;
        case 0xA6: /* Inverse off */
            s->inverse = 0;
            s->mode = ssd1306_IDLE;
            break;
        case 0xA7: /* Inverse on */
            s->inverse = 1;
            s->mode = ssd1306_IDLE;
            break;
        case 0xAE: /* Display off */
            s->enabled = 0;
            s->redraw = 1;
            s->mode = ssd1306_IDLE;
            break;
        case 0xAF: /* Display on */
            s->enabled = 1;
            s->redraw = 1;
            s->mode = ssd1306_IDLE;
            break;
        /* Unimplemented Commands */

        /* Scrolling Commands */
        case 0x26 ... 0x27: /* Continuous Horizontal Scroll Setup */
            // 6 data bytes
            if (s->cmd_byte_num == 0)
                s->mode = ssd1306_CMD_DATA;
            if (s->cmd_byte_num == 6)
                s->mode = ssd1306_IDLE;
            break;
        case 0x29 ... 0x2A: /* Continuous Vertical and Horizontal Scroll Setup */
            // 5 data bytes
            if (s->cmd_byte_num == 0)
                s->mode = ssd1306_CMD_DATA;
            if (s->cmd_byte_num == 5)
                s->mode = ssd1306_IDLE;
            break;
        case 0x2E: /* Deactivate Scroll */
            s->mode = ssd1306_IDLE;
            break;
        case 0x2F: /* Activate Scroll */
            s->mode = ssd1306_IDLE;
            break;
        case 0xA3: /* Set Vertical Scroll */
            // 2 data bytes
            if (s->cmd_byte_num == 0)
                s->mode = ssd1306_CMD_DATA;
            if (s->cmd_byte_num == 2)
                s->mode = ssd1306_IDLE;
            break;
        
        /* Address Setting Commands */
        case 0x00 ... 0x0F: /* Set Lower Column Start Address for Page Addressing Mode */
            s->col = (s->col & 0xf0) | (data & 0xf);
            s->mode = ssd1306_IDLE;
            break;
        case 0x10 ... 0x1F: /* Set Higher Column Start Address for Page Addressing Mode */
            s->col = (s->col & 0x0f) | (data & 0xf);
            s->mode = ssd1306_IDLE;
            break;
        case 0x20: /* Set Memory Addressing Mode */
            // 1 data byte
            if (s->cmd_byte_num == 0)
                s->mode = ssd1306_CMD_DATA;
            if (s->cmd_byte_num == 1)
                s->mode = ssd1306_IDLE;
            break;
        case 0x21: /* Set Column Address */
            // 2 data bytes
            if (s->cmd_byte_num == 0)
                s->mode = ssd1306_CMD_DATA;
            if (s->cmd_byte_num == 2)
                s->mode = ssd1306_IDLE;
            break;
        case 0x22: /* Set Page Address */
            // 2 data bytes
            if (s->cmd_byte_num == 0)
                s->mode = ssd1306_CMD_DATA;
            if (s->cmd_byte_num == 2)
                s->mode = ssd1306_IDLE;
            break;
        case 0xB0 ... 0xB7: /* Set Page Start Address for Page Addressing Mode */
            s->row = data & 0x07;
            s->mode = ssd1306_IDLE;
            break;
        
        /* Hardware Configuration Commands */
        case 0x40 ... 0x7F: /* Set Display Start Line */
            s->mode = ssd1306_IDLE;
            break;
        case 0xA0 ... 0xA1: /* Set Segment Re-map */
            s->mode = ssd1306_IDLE;
            break;
        case 0xA8: /* Set Multiplex Ratio */
            // 1 data byte
            if (s->cmd_byte_num == 0)
                s->mode = ssd1306_CMD_DATA;
            if (s->cmd_byte_num == 1)
                s->mode = ssd1306_IDLE;
            break;
        case 0xC0 ... 0xC8: /* Set COM Output Scan Direction */
            s->mode = ssd1306_IDLE;
            break;
        case 0xD3: /* Set Display Offset */
            // 1 data byte
            if (s->cmd_byte_num == 0)
                s->mode = ssd1306_CMD_DATA;
            if (s->cmd_byte_num == 1)
                s->mode = ssd1306_IDLE;
            break;
        case 0xDA: /* Set COM Pins Hardware Configuration */
            // 1 data byte
            if (s->cmd_byte_num == 0)
                s->mode = ssd1306_CMD_DATA;
            if (s->cmd_byte_num == 1)
                s->mode = ssd1306_IDLE;
            break;
        
        /* Timing and Driving Scheme Setting Commands */
        case 0xD5: /* Set Display Clock Divide Ratio/Oscillator Freq. */
            // 1 data byte
            if (s->cmd_byte_num == 0)
                s->mode = ssd1306_CMD_DATA;
            if (s->cmd_byte_num == 1)
                s->mode = ssd1306_IDLE;
            break;
        case 0xD9: /* Set Pre-charge Period */
            // 1 data byte
            if (s->cmd_byte_num == 0)
                s->mode = ssd1306_CMD_DATA;
            if (s->cmd_byte_num == 1)
                s->mode = ssd1306_IDLE;
            break;
        case 0xDB: /* Set Vcomh Deselect Level */
            // 1 data byte
            if (s->cmd_byte_num == 0)
                s->mode = ssd1306_CMD_DATA;
            if (s->cmd_byte_num == 1)
                s->mode = ssd1306_IDLE;
            break;
        case 0xE3: /* NOP */
            s->mode = ssd1306_IDLE;
            break;
        default:
            BADF("Unknown command: 0x%x\n", data);
        }
        break;
        s->cmd_byte_num++;
    }
    return 0;
}

static int ssd1306_event(I2CSlave *i2c, enum i2c_event event)
{
    ssd1306_state *s = SSD1306(i2c);

    switch (event) {
    case I2C_FINISH:
        s->mode = ssd1306_IDLE;
        break;
    case I2C_START_RECV:
    case I2C_START_SEND:
    case I2C_NACK:
        /* Nothing to do.  */
        break;
    default:
        return -1;
    }

    return 0;
}

static void ssd1306_update_display(void *opaque)
{
    ssd1306_state *s = (ssd1306_state *)opaque;
    DisplaySurface *surface = qemu_console_surface(s->con);
    uint8_t *dest_surface;

    if (!s->redraw)
        return;

    int bpp = surface_bits_per_pixel(surface);
    if (bpp != 32)
    {
        DPRINTF("Invalid surface bpp=%d\n", bpp);
        return;
    }

    dest_surface = surface_data(surface);
    if (s->flash)
    {
        memset(dest_surface, 0xff, FB_WIDTH * FB_HEIGHT * (bpp / 8));
    }
    else if (!s->enabled)
    {
        memset(dest_surface, 0x00, FB_WIDTH * FB_HEIGHT * (bpp / 8));
    }
    else
    {
        uint32_t surface_idx = 0;
        for (int h = 0; h < HEIGHT; h++)
            for (int hscale = 0; hscale < SCALE_FACTOR; hscale++)
                for (int w = 0; w < WIDTH; w++)
                    for (int bytes = 0; bytes < (bpp / 8) * SCALE_FACTOR; bytes++)
                        dest_surface[surface_idx++] = s->framebuffer[h * WIDTH + w];
    }
    s->redraw = 0;
    dpy_gfx_update(s->con, 0, 0, WIDTH, HEIGHT);
}

static void ssd1306_invalidate_display(void * opaque)
{
    ssd1306_state *s = (ssd1306_state *)opaque;
    s->redraw = 1;
}

static const VMStateDescription vmstate_ssd1306 = {
    .name = "ssd1306_oled",
    .version_id = 1,
    .minimum_version_id = 1,
    .fields = (const VMStateField[]) {
        VMSTATE_INT32(row, ssd1306_state),
        VMSTATE_INT32(col, ssd1306_state),
        VMSTATE_INT32(start_line, ssd1306_state),
        VMSTATE_INT32(mirror, ssd1306_state),
        VMSTATE_INT32(flash, ssd1306_state),
        VMSTATE_INT32(enabled, ssd1306_state),
        VMSTATE_INT32(inverse, ssd1306_state),
        VMSTATE_INT32(redraw, ssd1306_state),
        VMSTATE_UINT32(mode, ssd1306_state),
        VMSTATE_UINT8(cmd, ssd1306_state),
        VMSTATE_UINT8(cmd_byte_num, ssd1306_state),
        VMSTATE_BUFFER(framebuffer, ssd1306_state),
        VMSTATE_I2C_SLAVE(parent_obj, ssd1306_state),
        VMSTATE_END_OF_LIST()
    }
};

static const GraphicHwOps ssd1306_ops = {
    .invalidate  = ssd1306_invalidate_display,
    .gfx_update  = ssd1306_update_display,
};

static void ssd1306_realize(DeviceState *dev, Error **errp)
{
    ssd1306_state *s = SSD1306(dev);

    memset(s->framebuffer, 0, WIDTH * HEIGHT);
    s->con = graphic_console_init(dev, 0, &ssd1306_ops, s);
    qemu_console_resize(s->con, FB_WIDTH, FB_HEIGHT);
}

static void ssd1306_class_init(ObjectClass *klass, void *data)
{
    DeviceClass *dc = DEVICE_CLASS(klass);
    I2CSlaveClass *k = I2C_SLAVE_CLASS(klass);

    dc->realize = ssd1306_realize;
    k->event = ssd1306_event;
    k->recv = ssd1306_recv;
    k->send = ssd1306_send;
    dc->vmsd = &vmstate_ssd1306;
}

static const TypeInfo ssd1306_info = {
    .name          = TYPE_SSD1306,
    .parent        = TYPE_I2C_SLAVE,
    .instance_size = sizeof(ssd1306_state),
    .class_init    = ssd1306_class_init,
};

static void ssd1306_register_types(void)
{
    type_register_static(&ssd1306_info);
}

type_init(ssd1306_register_types)
